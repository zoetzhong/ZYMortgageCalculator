//
//  ZYMortgageCalculatorRateViewController.m
//  ZYMortgageCalculator
//
//  Created by Zoet on 2017/3/22.
//  Copyright © 2017年 wzwl. All rights reserved.
//
//  比例选择界面

#import "ZYMortgageCalculatorRateViewController.h"
#import "ZYMortgageCalculatorTableViewCell.h"

NSString* const kInputedCellIdentifier = @"InputedCellIdentifier";
NSString* const kSelectRateCellIdentifier = @"SelectCellIdentifier";

@interface ZYMortgageCalculatorRateViewController ()<UITableViewDelegate,UITableViewDataSource,ZYMortgageCalculatorTableViewCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView* tableView;/**< 列表 */

@property (assign,nonatomic) RateViewType rateViewType;/**< 界面类型 */
@property (assign,nonatomic) NSInteger totalPrice;/**< 首付 */
@property (assign,nonatomic) NSInteger downPaymen;/**< 首付 */
@property (assign,nonatomic) CGFloat baseLoanRate;/**< 基本贷款利率 */
@property (assign,nonatomic) CGFloat loanRate;/**< 贷款利率 */
@property (assign,nonatomic) CGFloat curretnRate;/**< 当前比例 */

@end

@implementation ZYMortgageCalculatorRateViewController{
    NSMutableDictionary* _dictRate;
    NSMutableArray* _arrayKey;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UITapGestureRecognizer* gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidenKeyboard)];
    gestureRecognizer.cancelsTouchesInView = NO;
    [_tableView addGestureRecognizer:gestureRecognizer];
}
-(void)setTotal:(NSInteger)totalPrice downPaymen:(NSInteger)downPaymen currentRate:(CGFloat)currentRate{
    _curretnRate = currentRate;
    _totalPrice = totalPrice;
    _downPaymen = downPaymen;
    _rateViewType = RateViewDownPaymen;
    _dictRate = [NSMutableDictionary dictionary];
    _arrayKey = [NSMutableArray array];
    for (NSInteger i=1; i<10; i++) {
        CGFloat rate = i *0.1f;
        [_arrayKey addObject:[NSString stringWithFormat:@"%.0f%%",rate*100]];
        [_dictRate setObject:@(rate) forKey:[NSString stringWithFormat:@"%.0f%%",rate*100]];
    }
    [_tableView reloadData];
}
-(void)setBaseLoanRate:(CGFloat)baseLoanRate loanRate:(CGFloat)loanRate currentRate:(CGFloat)currentRate{
    _baseLoanRate = baseLoanRate;
    _curretnRate = currentRate;
    _loanRate = loanRate;
    _rateViewType = RateViewLoanRate;
    _dictRate = [NSMutableDictionary dictionary];
    _arrayKey = [NSMutableArray array];
    for (NSInteger i = 70; i<=120; i+=5) {
        NSString* key;
        CGFloat rate = i*1.0f/100;
        if (i<100) {
            key = [NSString stringWithFormat:@"%.1f折",rate*10];
        }else if(i==100){
            key = @"基准利率";
        }else{
            key = [NSString stringWithFormat:@"%.2f倍",rate];
        }
        [_arrayKey addObject:key];
        [_dictRate setObject:@(rate) forKey:key];

    }
    for (CGFloat rate=0.7; rate<=1.2; rate=rate+0.05) {
            }
    [_tableView reloadData];
}

#pragma mark - UITableView Delegate & DataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 36;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return 1;
    }else{
        return [_arrayKey count];
    }
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = [indexPath section];
    NSInteger row = [indexPath row];
    if (section==0) {
        ZYMortgageCalculatorTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kInputedCellIdentifier];
        if (!cell){
            [tableView registerNib:[UINib nibWithNibName:@"ZYMortgageCalculatorTableViewCell" bundle:nil] forCellReuseIdentifier:kInputedCellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:kInputedCellIdentifier];
        }
        cell.delegate = self;
        if (_rateViewType==RateViewDownPaymen) {
            cell.labTitle.text = @"自定义首付金额";
            cell.fieldPrice.text = [NSString stringWithFormat:@"%ld",_downPaymen];

        }else{
            cell.labTitle.text = @"自定义基准利率";
            cell.fieldPrice.text = [NSString stringWithFormat:@"%.2f",_loanRate*100];
            cell.isInputFloat = YES;
            [cell setRateUnitView];

        }
        [cell.fieldPrice setHidden:NO];
        [cell.labContent setHidden:YES];
        return cell;
    }else{
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kSelectRateCellIdentifier];
        if (!cell){
            [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kSelectRateCellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:kSelectRateCellIdentifier];
        }
        NSString* key = [_arrayKey objectAtIndex:row];
        cell.textLabel.text = key;
        CGFloat rate = [[_dictRate objectForKey:key] floatValue];
        if (rate == _curretnRate) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = [indexPath section];
    NSInteger row = [indexPath row];
    ZYMortgageCalculatorTableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (section==0) {
        [cell.fieldPrice becomeFirstResponder];
    }else{
        NSString* key = [_arrayKey objectAtIndex:row];
        CGFloat rate = [[_dictRate objectForKey:key] floatValue];
        _curretnRate = rate;
        if (_rateViewType==RateViewDownPaymen) {
            _downPaymen = _totalPrice * _curretnRate;
            if(self.delegate&&[self.delegate respondsToSelector:@selector(didSelectDownPaymen:rateDesc:)]){
                [self.delegate didSelectDownPaymen:_downPaymen rateDesc:key];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }else{
            if(self.delegate&&[self.delegate respondsToSelector:@selector(didSelectLoanRateDiscount:rateDiscountDesc:)]){
                [self.delegate didSelectLoanRateDiscount:rate rateDiscountDesc:key];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 18.0f;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView* viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    UILabel* labTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 18)];
    labTitle.font = [UIFont systemFontOfSize:13.0f];
    labTitle.textColor = [UIColor lightGrayColor];
    [viewHeader addSubview:labTitle];
    if (section==0) {
        labTitle.text = @"";
    }else{
        if (_rateViewType==RateViewDownPaymen) {
            labTitle.text = @"选择首付比例";
        }else{
            labTitle.text = @"选择贷款利率";
        }
    }
    return viewHeader;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self hidenKeyboard];
}
-(void)hidenKeyboard{
    ZYMortgageCalculatorTableViewCell* cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (cell){
        [cell closeKeyboard];
    }
}
#pragma mark - Custome Delegate
-(void)didUpdateValue:(NSInteger)value{
    ZYMortgageCalculatorTableViewCell* cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (value<=0){
        cell.fieldPrice.text = [NSString stringWithFormat:@"%ld",_downPaymen];
        return;
    }else{
        if (value > _totalPrice) {
            _downPaymen = _totalPrice;
            cell.fieldPrice.text = [NSString stringWithFormat:@"%ld",_downPaymen];
        }else{
            _downPaymen = value;
        }
    }
    CGFloat rate = _downPaymen/_totalPrice;
    _curretnRate = rate;
    if(self.delegate&&[self.delegate respondsToSelector:@selector(didSelectDownPaymen:rateDesc:)]){
        [self.delegate didSelectDownPaymen:_downPaymen rateDesc:@"自定义"];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)didUpdateRateValue:(CGFloat)rateValue{
    ZYMortgageCalculatorTableViewCell* cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (rateValue<=0){
        cell.fieldPrice.text = [NSString stringWithFormat:@"%.2f",_loanRate*100];
    }
    _curretnRate = rateValue*1.0f/100;
    CGFloat newDiscount = _curretnRate/_baseLoanRate;
    if(self.delegate&&[self.delegate respondsToSelector:@selector(didSelectLoanRateDiscount:rateDiscountDesc:)]){
        [self.delegate didSelectLoanRateDiscount:newDiscount rateDiscountDesc:@"自定义"];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
