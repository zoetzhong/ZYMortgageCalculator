//
//  ZYMortgageCalculatorRateViewController.h
//  ZYMortgageCalculator
//
//  Created by Zoet on 2017/3/22.
//  Copyright © 2017年 wzwl. All rights reserved.
//
//  比例选择界面

#import <UIKit/UIKit.h>

/**
 界面设置类型

 - RateViewDownPaymen: 首付比例
 - RateViewLoanRate: 贷款利率
 */
typedef NS_ENUM(NSInteger,RateViewType) {
    RateViewDownPaymen=0,
    RateViewLoanRate
};

@protocol ZYMortgageCalculatorRateViewControllerDelegate <NSObject>

/**
 选择了贷款利率

 @param rateDiscount 利率数值
 @param rateDiscountDesc 利率文字描述
 */
-(void)didSelectLoanRateDiscount:(CGFloat)rateDiscount rateDiscountDesc:(NSString*)rateDiscountDesc;

/**
 选择了首付比例

 @param downPaymen 自定义首付
 @param rateDesc 比例文字描述
 */
-(void)didSelectDownPaymen:(NSInteger)downPaymen rateDesc:(NSString*)rateDesc;

@end

@interface ZYMortgageCalculatorRateViewController : UIViewController
@property (strong,nonatomic) id<ZYMortgageCalculatorRateViewControllerDelegate> delegate;
-(void)setTotal:(NSInteger)totalPrice downPaymen:(NSInteger)downPaymen currentRate:(CGFloat)currentRate;
-(void)setBaseLoanRate:(CGFloat)baseLoanRate loanRate:(CGFloat)loanRate currentRate:(CGFloat)currentRate;
@end
