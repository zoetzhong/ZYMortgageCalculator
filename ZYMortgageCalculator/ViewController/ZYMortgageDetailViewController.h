//
//  ZYMortgageDetailViewController.h
//  ZYMortgageCalculator
//
//  Created by Zoet on 2017/3/27.
//  Copyright © 2017年 wzwl. All rights reserved.
//
//  房贷计算结果详情


#import <UIKit/UIKit.h>

@interface ZYMortgageDetailViewController : UIViewController
@property (assign,nonatomic) NSInteger downPaymen;/**< 首付 */
@property (assign,nonatomic) NSInteger totalLoanAmount;/**< 贷款总额 */
@property (assign,nonatomic) CGFloat totalInterest;/**< 利息总计 */
@property (strong,nonatomic) NSArray* arrayDetail;/**< 数据详情 */
@end
