//
//  ZYMortgageCalculatorViewController.h
//  ZYMortgageCalculator
//
//  Created by Zoet on 2017/3/22.
//  Copyright © 2017年 wzwl. All rights reserved.
//
//  房贷计算器

#import <UIKit/UIKit.h>

@interface ZYMortgageCalculatorViewController : UIViewController

/**
 设置总价和利率

 @param totalPrice 总价
 @param loanRate 贷款利率
 @param fundRate 公积金贷款利率
 */
-(void)setTotalPrice:(NSInteger)totalPrice loanRate:(CGFloat)loanRate fundRate:(CGFloat)fundRate;
@end
