//
//  ZYMortgageDetailViewController.m
//  ZYMortgageCalculator
//
//  Created by Zoet on 2017/3/27.
//  Copyright © 2017年 wzwl. All rights reserved.
//
//  房贷计算结果详情

#import "ZYMortgageDetailViewController.h"
#import "ZYMortgageDetailTableViewCell.h"
#import "ZYMortgageModel.h"

@interface ZYMortgageDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel* labDownPaymen;/**< 首付 */
@property (weak, nonatomic) IBOutlet UILabel* labTotalLoanAmount;/**< 贷款总额 */
@property (weak, nonatomic) IBOutlet UILabel* labTotalInterest;/**< 利息总计 */
@property (weak, nonatomic) IBOutlet UITableView* tableView;/**< 详情列表 */
@end

@implementation ZYMortgageDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"月供明细";
    [_labDownPaymen setText:[NSString stringWithFormat:@"%ld万",_downPaymen]];
    [_labTotalLoanAmount setText:[NSString stringWithFormat:@"%ld万",_totalLoanAmount]];
    [_labTotalInterest setText:[NSString stringWithFormat:@"%.2f万",_totalInterest]];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setArrayDetail:(NSArray *)arrayDetail{
    _arrayDetail = arrayDetail;
    [_tableView reloadData];
}
#pragma mark - UITableView Delegate & DataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 30;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return [_arrayDetail count]/12.0f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 12;
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ZYMortgageDetailTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    if (!cell){
        [tableView registerNib:[UINib nibWithNibName:@"ZYMortgageDetailTableViewCell" bundle:nil] forCellReuseIdentifier:@"CellIdentifier"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier"];
    }
    NSInteger row = [indexPath row];
    NSInteger section = [indexPath section];
    NSInteger dataIndex = section*12+row;
    ZYMortgageModel* model = [_arrayDetail objectAtIndex:dataIndex];
    cell.labMonth.text = [NSString stringWithFormat:@"%ld",model.month];
    cell.labPaymentCapital.text = [NSString stringWithFormat:@"%.0f",round(model.monthPaymentCapital * 100) / 100];
    cell.labPaymentInterest.text = [NSString stringWithFormat:@"%.0f",round(model.monthPaymentInterest * 100) / 100];
    cell.labPayment.text = [NSString stringWithFormat:@"%.0f",round(model.monthPayment * 100) / 100];
    cell.labSurplusCapital.text = [NSString stringWithFormat:@"%.0f",round(model.surplusCapital * 100) / 100];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 30.0f;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 36.0f)];
    [view setBackgroundColor:[UIColor colorWithRed:246.0f/255.0f green:246.0f/255.0f blue:246.0f/255.0f alpha:1.0f]];
    
    UILabel* title = [[UILabel alloc] initWithFrame:CGRectMake(24, 0, 200, 30.0f)];
    [title setText:[NSString stringWithFormat:@"第%ld年",(section +1)]];
    [title setFont:[UIFont boldSystemFontOfSize:15.0f]];
    
    
    [view addSubview:title];
    return view;
}

@end
