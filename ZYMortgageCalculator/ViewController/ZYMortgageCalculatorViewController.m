//
//  ZYMortgageCalculatorViewController.m
//  ZYMortgageCalculator
//
//  Created by Zoet on 2017/3/22.
//  Copyright © 2017年 wzwl. All rights reserved.
//
//  房贷计算器

#import "ZYMortgageCalculatorViewController.h"
#import "ZYMortgageCalculatorTableViewCell.h"
#import "ZYMortgageCalculatorRateViewController.h"
#import "ZYMortagageCalculateUtil.h"
#import "ZYCalculateResultTableViewCell.h"
#import "ZYMortgageDetailViewController.h"

NSString* const kCellIdentifier = @"CellIdentifier";
NSString* const kCalculateResultCellIdentifier = @"CalculateResultCellIdentifier";
NSInteger const kYearMax = 30;/**< 最高年限 */

/**
 计算结果类型

 - CalculateResultAverageCapitPlusInterest: 等额本息无时间差
 - CalculateResultAverageCapital: 等额本金无时间差
 - CalculateResultAverageCapitPlusInterestTimeDiff: 等额本息有时间差
 - CalculateResultAverageCapitalTimeDiff: 等额本金无时间差
 */
typedef NS_ENUM(NSInteger,CalculateResultType){
    CalculateResultAverageCapitPlusInterest=0,
    CalculateResultAverageCapital,
    CalculateResultAverageCapitPlusInterestTimeDiff,
    CalculateResultAverageCapitalTimeDiff
};

@interface ZYMortgageCalculatorViewController ()<UITableViewDelegate,UITableViewDataSource,ZYMortgageCalculatorTableViewCellDelegate,ZYMortgageCalculatorRateViewControllerDelegate,UIPickerViewDataSource, UIPickerViewDelegate,ZYCalculateResultTableViewCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView* tableView;/**< tableView */

@property (assign, nonatomic) NSInteger totalPrice;/**< 总价 */
@property (assign, nonatomic) NSInteger downPaymen;/**< 首付 */
@property (assign, nonatomic) NSInteger totalLoanAmount;/**< 贷款金额 */
@property (assign, nonatomic) CGFloat downPaymenRate;/**< 首付比例，默认40% */
@property (assign, nonatomic) CGFloat loanRateDiscount;/**< 贷款利率折扣 */
@property (assign, nonatomic) CGFloat fundRateDiscount;/**< 公积金贷款利率折扣 */
@property (strong, nonatomic) NSString* loanRateDiscountDesc;/**< 贷款利率折扣文字描述 */
@property (strong, nonatomic) NSString* fundRateDiscountDesc;/**< 公积金贷款利率折扣文字描述 */
@property (assign, nonatomic) CGFloat baseLoanRate;/**< 基本商业贷款利率 */
@property (assign, nonatomic) CGFloat baseFundRate;/**< 基本公积金贷款利率 */
@property (assign, nonatomic) CGFloat loanRate;/**< 商业贷款利率 */
@property (assign, nonatomic) CGFloat fundRate;/**< 公积金利率 */
@property (assign, nonatomic) NSInteger loanYear;/**< 商业贷款年限 */
@property (assign, nonatomic) NSInteger fundYear;/**< 公积金年限 */
@property (assign, nonatomic) NSInteger loanAmount;/**< 商业贷款金额 */
@property (assign, nonatomic) NSInteger fundAmount;/**< 公积金贷款金额，默认18万 */
@end

@implementation ZYMortgageCalculatorViewController{
    NSArray* _arrayAmount;/**< 金额 */
    NSArray* _arrayYear;/**< 年限 */
    BOOL _isSetFunc;/**< 是否是设置公积金 */
    BOOL _isSetFuncRate;/**< 是否是设置公积金利率 */
    BOOL _isCalculatDone;/**< 是否计算完成 */
    
    CalculateResultType _resultType;/**< 计算结果类型 */
    NSDictionary* _dictResult;/**< 计算结果 */

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"房贷计算器";
    UITapGestureRecognizer* gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hidenKeyboard)];
    gestureRecognizer.cancelsTouchesInView = NO;
    [_tableView addGestureRecognizer:gestureRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - View
/**
 设置总价和利率
 
 @param totalPrice 总价(单位：万元)
 @param loanRate 贷款利率
 @param fundRate 公积金贷款利率
 */
-(void)setTotalPrice:(NSInteger)totalPrice loanRate:(CGFloat)loanRate fundRate:(CGFloat)fundRate{
    _totalPrice = totalPrice;
    _loanRate = loanRate;
    _baseLoanRate = _loanRate;
    _fundRate = fundRate;
    _baseFundRate = _fundRate;
    _downPaymenRate = 0.4f;//默认40%
    _downPaymen = _totalPrice *_downPaymenRate;//首付金额
    _totalLoanAmount = _totalPrice - _downPaymen;//贷款总金额
    _fundAmount = 18;
    _loanAmount = _totalLoanAmount - _fundAmount;
    _fundYear = kYearMax;
    _loanYear = kYearMax;
    _loanRateDiscount = 1.0f;
    _fundRateDiscount = 1.0f;
    _loanRateDiscountDesc = @"基准利率";
    _fundRateDiscountDesc = @"基准利率";
    
    [_tableView reloadData];
}
/**
 根据更新贷款金额价格界面
 
 */
-(void)updateLoanAmountViews{
    _totalLoanAmount = _totalPrice - _downPaymen;//贷款总金额
    if (_fundAmount>_totalLoanAmount) {
        _fundAmount = _totalLoanAmount;
    }
    _loanAmount = _totalLoanAmount - _fundAmount;
    [_tableView reloadData];
}

/**
 显示选择控件
 */
- (void)showPickerViewIsSetFunc:(BOOL)isSetFunc{
    [self setupTimePickerViewData];
    UIPickerView* pickView = [[UIPickerView alloc]init];
    pickView.frame = CGRectMake(0, 0, self.view.frame.size.width-20, 240);
    pickView.delegate = self;
    pickView.dataSource = self;
    if(isSetFunc){
        [pickView selectRow:_fundAmount inComponent:0 animated:NO];
        [pickView selectRow:_fundYear-1 inComponent:1 animated:NO];
    }else{
        [pickView selectRow:_loanAmount inComponent:0 animated:NO];
        [pickView selectRow:_loanYear-1 inComponent:1 animated:NO];
    }
    _isSetFunc = isSetFunc;
    
    UIAlertController* alertVC = [UIAlertController alertControllerWithTitle:@"\n\n\n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alertVC.view addSubview:pickView];
    
    UIAlertAction *closeAction = [UIAlertAction actionWithTitle:@"关闭" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alertVC addAction:closeAction];
    
    [self presentViewController:alertVC animated:YES completion:nil];
    
}
#pragma mark - Action
-(IBAction)onCalculateButtonClicked:(id)sender{
    if (!_isCalculatDone) {
        if(_loanYear==_fundYear){
            _resultType = CalculateResultAverageCapitPlusInterest;/**< 默认等额本息 */
        }else{
            _resultType = CalculateResultAverageCapitPlusInterestTimeDiff;/**< 默认等额本息 */
        }
    }else{
        if (_resultType==CalculateResultAverageCapitPlusInterest||_resultType==CalculateResultAverageCapitPlusInterestTimeDiff) {
            //等额本息
            if(_loanYear==_fundYear){
                _resultType = CalculateResultAverageCapitPlusInterest;
            }else{
                if (_loanAmount>0&&_fundAmount>0) {
                    _resultType = CalculateResultAverageCapitPlusInterestTimeDiff;
                }else{
                    _resultType = CalculateResultAverageCapitPlusInterest;
                }
            }
        }else{
            //等额本金
            if(_loanYear==_fundYear){
                _resultType = CalculateResultAverageCapital;
            }else{
                if (_loanAmount>0&&_fundAmount>0) {
                    _resultType = CalculateResultAverageCapitalTimeDiff;
                }else{
                    _resultType = CalculateResultAverageCapital;
                }
            }
        }
    }
    if (_resultType==CalculateResultAverageCapitPlusInterest||_resultType==CalculateResultAverageCapitPlusInterestTimeDiff) {
        //等额本息
        _dictResult = [[ZYMortagageCalculateUtil shareInstance] calculateAverageCapitPlusInterestWithTotalLoanAmount:_totalLoanAmount*10000 loanAmount:_loanAmount*10000 loanRate:_loanRate loanYear:_loanYear fundAmount:_fundAmount*10000 fundRate:_fundRate fundYear:_fundYear];

    }else{
        //等额本金
        _dictResult = [[ZYMortagageCalculateUtil shareInstance] calculateAverageCapitalWithTotalLoanAmount:_totalLoanAmount*10000 loanAmount:_loanAmount*10000 loanRate:_loanRate loanYear:_loanYear fundAmount:_fundAmount*10000 fundRate:_fundRate fundYear:_fundYear];

    }
    _isCalculatDone = YES;
    [_tableView reloadData];
}
#pragma mark - Request

#pragma mark - Delegate
#pragma mark - UITableView Delegate & DataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = [indexPath section];
    if (section==2) {
        if(_resultType == CalculateResultAverageCapitPlusInterest){
            return 175;
        }else if(_resultType == CalculateResultAverageCapitPlusInterestTimeDiff){
            return 200;
        }else if(_resultType == CalculateResultAverageCapital){
            return 200;
        }else if(_resultType == CalculateResultAverageCapitalTimeDiff){
            return 250;
        }
        return 175;
    }else{
        return 44;
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 3;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section==0) {
        return 2;
    }else if(section == 1){
        return 4;
    }else{
        return 1;
    }
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = [indexPath section];
    NSInteger row = [indexPath row];
    
    if (section < 2) {
        ZYMortgageCalculatorTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
        if (!cell){
            [tableView registerNib:[UINib nibWithNibName:@"ZYMortgageCalculatorTableViewCell" bundle:nil] forCellReuseIdentifier:kCellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
        }
        cell.delegate = self;
        
        if (section==0) {
            if (row==0) {
                cell.labTitle.text = @"总价";
                [cell.fieldPrice setHidden:NO];
                [cell.labContent setHidden:YES];
                cell.fieldPrice.text = [NSString stringWithFormat:@"%ld",_totalPrice];
            }else{
                cell.labTitle.text = @"首付比例";
                [cell.fieldPrice setHidden:YES];
                [cell.labContent setHidden:NO];
                NSInteger rate = _downPaymenRate*100;
                if (rate%10==0) {
                    cell.labContent.text = [NSString stringWithFormat:@"%.0f%%(%ld万)",_downPaymenRate*100,_downPaymen];
                }else{
                    cell.labContent.text = [NSString stringWithFormat:@"自定义(%ld万)",_downPaymen];
                }
            }
        }else{
            [cell.fieldPrice setHidden:YES];
            [cell.labContent setHidden:NO];
            if (row == 0) {
                cell.labTitle.text = @"公积金贷款";
                cell.labContent.text = [NSString stringWithFormat:@"%ld 万    年限 %ld 年",_fundAmount,_fundYear];
            }else if (row == 1) {
                cell.labTitle.text = @"公积金贷款利率";
                cell.labContent.text = [NSString stringWithFormat:@"%@(%.2f%%)",_fundRateDiscountDesc,_fundRate*100];
            }else if (row == 2) {
                cell.labTitle.text = @"商业贷款";
                cell.labContent.text = [NSString stringWithFormat:@"%ld 万    年限 %ld 年",_loanAmount,_loanYear];
            }else if (row == 3) {
                cell.labTitle.text = @"商业贷款利率";
                cell.labContent.text = [NSString stringWithFormat:@"%@(%.2f%%)",_loanRateDiscountDesc,_loanRate*100];
            }
        }
        
        
        return cell;
    }else{
        ZYCalculateResultTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kCalculateResultCellIdentifier];
        if (!cell){
            [tableView registerNib:[UINib nibWithNibName:@"ZYCalculateResultTableViewCell" bundle:nil] forCellReuseIdentifier:kCalculateResultCellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:kCalculateResultCellIdentifier];
        }
        cell.delegate = self;
        if(_isCalculatDone){
            [cell setHidden:NO];
        }else{
            [cell setHidden:YES];
        }
        if (_resultType== CalculateResultAverageCapitPlusInterest) {
            [cell setDownPaymen:_downPaymen
                totalLoanAmount:_totalLoanAmount
                  totalInterest:[[_dictResult objectForKey:kTotalInterestKey] integerValue]/10000
                   monthPayment:[[_dictResult objectForKey:kMonthPayment] floatValue]];
        }else if(_resultType== CalculateResultAverageCapitPlusInterestTimeDiff){
            [cell setDownPaymen:_downPaymen
                totalLoanAmount:_totalLoanAmount
                  totalInterest:[[_dictResult objectForKey:kTotalInterestKey] integerValue]/10000
                     beforeYear:[[_dictResult objectForKey:kBeforeYearKey] integerValue]
             beforeMonthPayment:[[_dictResult objectForKey:kBeforeYearMonthPaymentKey] floatValue]
                      afterYear:[[_dictResult objectForKey:kAfterYearKey] integerValue]
              afterMonthPayment:[[_dictResult objectForKey:kAfterYearMonthPaymentKey] floatValue]];
        }else if(_resultType== CalculateResultAverageCapital){
            [cell setDownPaymen:_downPaymen
                totalLoanAmount:_totalLoanAmount
                  totalInterest:[[_dictResult objectForKey:kTotalInterestKey] integerValue]/10000
                   monthPayment:[[_dictResult objectForKey:kMonthPayment] floatValue]
                  monthDecrease:[[_dictResult objectForKey:kMonthDecreaseKey] floatValue]];
        }else if(_resultType== CalculateResultAverageCapitalTimeDiff){
            [cell setDownPaymen:_downPaymen
                totalLoanAmount:_totalLoanAmount
                  totalInterest:[[_dictResult objectForKey:kTotalInterestKey] integerValue]/10000
             beforeMonthPayment:[[_dictResult objectForKey:kBeforeYearMonthPaymentKey] floatValue]
                 beforeDecrease:[[_dictResult objectForKey:kBeforeMonthDecreaseKey] floatValue]
                     afterMonth:[[_dictResult objectForKey:kBeforeYearKey] integerValue]*12+1
              afterMonthPayment:[[_dictResult objectForKey:kAfterYearMonthPaymentKey] floatValue]
                  afterDecrease:[[_dictResult objectForKey:kAfterMonthDecreaseKey] floatValue]];
        }
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = [indexPath section];
    NSInteger row = [indexPath row];
    ZYMortgageCalculatorTableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (section==0) {
        if (row==0) {
            [cell.fieldPrice becomeFirstResponder];
        }else{
            ZYMortgageCalculatorRateViewController * rateVC = [[ZYMortgageCalculatorRateViewController alloc] initWithNibName:@"ZYMortgageCalculatorRateViewController" bundle:nil];
            [rateVC setTotal:_totalPrice downPaymen:_downPaymen currentRate:_downPaymenRate];
            rateVC.delegate = self;
            [self.navigationController pushViewController:rateVC animated:YES];
        }
    }else if(section==1){
        if (row == 0) {
            [self showPickerViewIsSetFunc:YES];
        }else if (row == 1) {
            ZYMortgageCalculatorRateViewController * rateVC = [[ZYMortgageCalculatorRateViewController alloc] initWithNibName:@"ZYMortgageCalculatorRateViewController" bundle:nil];
            [rateVC setBaseLoanRate:_baseFundRate loanRate:_fundRate currentRate:_fundRateDiscount];
            rateVC.delegate = self;
            _isSetFuncRate = YES;
            [self.navigationController pushViewController:rateVC animated:YES];
        }else if (row == 2) {
            [self showPickerViewIsSetFunc:NO];
        }else if (row == 3) {
            ZYMortgageCalculatorRateViewController * rateVC = [[ZYMortgageCalculatorRateViewController alloc] initWithNibName:@"ZYMortgageCalculatorRateViewController" bundle:nil];
            [rateVC setBaseLoanRate:_baseLoanRate loanRate:_loanRate currentRate:_loanRateDiscount];
            rateVC.delegate = self;
            _isSetFuncRate = NO;
            [self.navigationController pushViewController:rateVC animated:YES];
        }
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 18.0f;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView* viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    UILabel* labTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 18)];
    labTitle.font = [UIFont systemFontOfSize:13.0f];
    labTitle.textColor = [UIColor lightGrayColor];
    [viewHeader addSubview:labTitle];
    if (section==0) {
        labTitle.text = @"";
    }else if (section==1){
        labTitle.text = [NSString stringWithFormat:@"贷款总额%ld万",_totalLoanAmount];
    }else {
        labTitle.text = @"";
    }
    return viewHeader;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 18.0f;
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView* viewFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    UILabel* labTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, tableView.frame.size.width, 18)];
    labTitle.font = [UIFont systemFontOfSize:13.0f];
    labTitle.textColor = [UIColor lightGrayColor];
    [viewFooter addSubview:labTitle];
    if (section==0) {
        labTitle.text = @"";
    }else if (section==1){
        labTitle.text = [NSString stringWithFormat:@"贷款基准利率：公积金%.2f%%，商业%.2f%%",_baseFundRate*100,_baseLoanRate*100];
    }else {
        if(_isCalculatDone){
            [labTitle setHidden:NO];
        }else{
            [labTitle setHidden:YES];
        }
        labTitle.text = @"以上结果仅供参考，请以当地实际情况为准";
    }
    return viewFooter;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self hidenKeyboard];
}
-(void)hidenKeyboard{
    ZYMortgageCalculatorTableViewCell* cell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if (cell){
        [cell closeKeyboard];
    }
}
#pragma mark - Picker Delegate
-(void)setupTimePickerViewData{
    NSMutableArray* arrayTempYear = [NSMutableArray array];
    for (NSInteger year = 1 ; year<=kYearMax;year++) {
        [arrayTempYear addObject:[NSString stringWithFormat:@"年限 %ld 年",year]];
    }
    _arrayYear = [NSArray arrayWithArray:arrayTempYear];
    
    NSMutableArray* arrayTempAmount = [NSMutableArray array];
    for (NSInteger amount = 0 ; amount<=_totalLoanAmount ;amount++) {
        [arrayTempAmount addObject:[NSString stringWithFormat:@"%ld 万",amount]];
    }
    _arrayAmount = [NSArray arrayWithArray:arrayTempAmount];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return _arrayAmount.count;
    } else{
        return _arrayYear.count;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (component == 0) {
        return [_arrayAmount objectAtIndex:row];
    } else{
        return [_arrayYear objectAtIndex:row];
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    return self.view.frame.size.width/2;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(_isSetFunc){
        //公积金
        if (component==0) {
            _fundAmount = row;
        }else{
            _fundYear = row+1;
        }
        ZYMortgageCalculatorTableViewCell* fundCell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        fundCell.labContent.text = [NSString stringWithFormat:@"%ld 万    年限 %ld 年",_fundAmount,_fundYear];
        
        //关联加减
        _loanAmount = _totalLoanAmount - _fundAmount;
        ZYMortgageCalculatorTableViewCell* loanCell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1]];
        loanCell.labContent.text = [NSString stringWithFormat:@"%ld 万    年限 %ld 年",_loanAmount,_loanYear];

        
    }else{
        //商业贷款
        if (component==0) {
            _loanAmount = row;
        }else{
            _loanYear = row+1;
        }
        ZYMortgageCalculatorTableViewCell* loanCell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1]];
        loanCell.labContent.text = [NSString stringWithFormat:@"%ld 万    年限 %ld 年",_loanAmount,_loanYear];
        //关联加减
        _fundAmount = _totalLoanAmount - _loanAmount;
        ZYMortgageCalculatorTableViewCell* fundCell = [_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        fundCell.labContent.text = [NSString stringWithFormat:@"%ld 万    年限 %ld 年",_fundAmount,_fundYear];
    }
}


#pragma mark - Custome Delegate
-(void)didUpdateValue:(NSInteger)value{
    _totalPrice = value;
    _downPaymen = _totalPrice*_downPaymenRate;

    [self updateLoanAmountViews];
}
-(void)didSelectDownPaymen:(NSInteger)downPaymen rateDesc:(NSString *)rateDesc{
    _downPaymen = downPaymen;
    _downPaymenRate = downPaymen*1.0f/_totalPrice;
    [self updateLoanAmountViews];
}
-(void)didSelectLoanRateDiscount:(CGFloat)rateDiscount rateDiscountDesc:(NSString *)rateDiscountDesc{
    if (_isSetFuncRate) {
        _fundRateDiscount = rateDiscount;
        _fundRateDiscountDesc = rateDiscountDesc;
        _fundRate = _baseFundRate * rateDiscount;
    }else{
        _loanRateDiscount = rateDiscount;
        _loanRateDiscountDesc = rateDiscountDesc;
        _loanRate = _baseLoanRate * rateDiscount;
    }
    [_tableView reloadData];
}
#pragma mark - WZCalculateResultTableViewCellDelegate
-(void)didChangeSegmentToIndex:(NSInteger)index{
    if (index==0) {
        if(_loanYear==_fundYear){
            _resultType = CalculateResultAverageCapitPlusInterest;
        }else{
            if (_loanAmount>0&&_fundAmount>0) {
                _resultType = CalculateResultAverageCapitPlusInterestTimeDiff;
            }else{
                _resultType = CalculateResultAverageCapitPlusInterest;
            }
        }
    }else{
        if(_loanYear==_fundYear){
            _resultType = CalculateResultAverageCapital;
        }else{
            if (_loanAmount>0&&_fundAmount>0) {
                _resultType = CalculateResultAverageCapitalTimeDiff;
            }else{
                _resultType = CalculateResultAverageCapital;
            }
        }
    }
    if (_resultType==CalculateResultAverageCapitPlusInterest||_resultType==CalculateResultAverageCapitPlusInterestTimeDiff) {
        //等额本息
        _dictResult = [[ZYMortagageCalculateUtil shareInstance] calculateAverageCapitPlusInterestWithTotalLoanAmount:_totalLoanAmount*10000 loanAmount:_loanAmount*10000 loanRate:_loanRate loanYear:_loanYear fundAmount:_fundAmount*10000 fundRate:_fundRate fundYear:_fundYear];
    }else{
        //等额本金
        _dictResult = [[ZYMortagageCalculateUtil shareInstance] calculateAverageCapitalWithTotalLoanAmount:_totalLoanAmount*10000 loanAmount:_loanAmount*10000 loanRate:_loanRate loanYear:_loanYear fundAmount:_fundAmount*10000 fundRate:_fundRate fundYear:_fundYear];
    }
    _isCalculatDone = YES;
    [_tableView reloadData];
}
-(void)didSelectedCheckDetail{
    ZYMortgageDetailViewController* vc = [[ZYMortgageDetailViewController alloc] initWithNibName:@"ZYMortgageDetailViewController" bundle:nil];
    vc.downPaymen = _downPaymen;
    vc.totalLoanAmount = _totalLoanAmount;
    vc.totalInterest = [[_dictResult objectForKey:kTotalInterestKey] floatValue]*1.0f/10000.0f;
    vc.arrayDetail = [_dictResult objectForKey:kArrayMortgageKey];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
