//
//  SceneDelegate.h
//  ZYMortgageCalculator
//
//  Created by Zoet on 2019/12/20.
//  Copyright © 2019 zoet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

