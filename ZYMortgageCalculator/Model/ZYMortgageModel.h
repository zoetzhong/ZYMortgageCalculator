//
//  ZYMortgageModel.h
//  ZYMortgageCalculator
//
//  Created by Zoet on 2017/3/23.
//  Copyright © 2017年 wzwl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ZYMortgageModel : NSObject
@property (assign,nonatomic) NSInteger month;/**< 期数 */
@property (assign,nonatomic) CGFloat monthPaymentCapital;/**< 月供本金 */
@property (assign,nonatomic) CGFloat monthPaymentInterest;/**< 月供利息 */
@property (assign,nonatomic) CGFloat monthPayment;/**< 月供 */
@property (assign,nonatomic) CGFloat surplusCapital;/**< 剩余本金 */
@end
