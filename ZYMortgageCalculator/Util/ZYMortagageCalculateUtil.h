//
//  ZYMortagageCalculateUtil.h
//  ZYMortgageCalculator
//
//  Created by Zoet on 2017/3/23.
//  Copyright © 2017年 wzwl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern NSString* const kTotalAmountKey;/**< 贷款总额 */
extern NSString* const kTotalInterestKey;/**< 总利息 */
extern NSString* const kMonthDecreaseKey;/**< 每月月供递减，等额本金 */
extern NSString* const kArrayMortgageKey;/**< 每月还款记录 */
extern NSString* const kMonthPayment;/**< 月供 */
extern NSString* const kBeforeYearKey;/**< 前x年 */
extern NSString* const kBeforeYearMonthPaymentKey;/**< 前x年月供 */
extern NSString* const kBeforeMonthDecreaseKey;/**< 前x年月递减 */
extern NSString* const kAfterYearKey;/**< 后x年 */
extern NSString* const kAfterYearMonthPaymentKey;/**< 后x年月供 */
extern NSString* const kAfterMonthDecreaseKey;/**< 后x年月递减 */

@interface ZYMortagageCalculateUtil : NSObject
+ (instancetype)shareInstance;

/**
 计算等额本金贷款

 @param totalAmount 贷款总额
 @param loanAmount 商业贷金额
 @param loanRate 商业贷利率
 @param loanYear 商业贷年限
 @param fundAmount 公积金贷款金额
 @param fundRate 公积金贷款利率
 @param fundYear 公积金贷款年限
 @return 结果
 */
-(NSDictionary*)calculateAverageCapitalWithTotalLoanAmount:(NSInteger)totalAmount loanAmount:(NSInteger)loanAmount loanRate:(CGFloat)loanRate loanYear:(NSInteger)loanYear fundAmount:(NSInteger)fundAmount fundRate:(CGFloat)fundRate fundYear:(NSInteger)fundYear;
/**
 计算等额本息贷款
 
 @param totalAmount 贷款总额
 @param loanAmount 商业贷金额
 @param loanRate 商业贷利率
 @param loanYear 商业贷年限
 @param fundAmount 公积金贷款金额
 @param fundRate 公积金贷款利率
 @param fundYear 公积金贷款年限
 @return 结果
 */
-(NSDictionary*)calculateAverageCapitPlusInterestWithTotalLoanAmount:(NSInteger)totalAmount loanAmount:(NSInteger)loanAmount loanRate:(CGFloat)loanRate loanYear:(NSInteger)loanYear fundAmount:(NSInteger)fundAmount fundRate:(CGFloat)fundRate fundYear:(NSInteger)fundYear;
@end
