//
//  ZYMortagageCalculateUtil.m
//  ZYMortgageCalculator
//
//  Created by Zoet on 2017/3/23.
//  Copyright © 2017年 wzwl. All rights reserved.
//

#import "ZYMortagageCalculateUtil.h"
#import "ZYMortgageModel.h"

NSString* const kTotalAmountKey = @"totalAmount";
NSString* const kTotalInterestKey = @"totalInterest";
NSString* const kMonthDecreaseKey = @"monthDecrease";
NSString* const kArrayMortgageKey = @"arrayMortgage";
NSString* const kMonthPayment = @"monthPayment";

NSString* const kBeforeYearKey = @"BeforeYear";
NSString* const kBeforeYearMonthPaymentKey = @"BeforeYearMonthPayment";
NSString* const kBeforeMonthDecreaseKey = @"BeforeMonthDecrease";

NSString* const kAfterYearKey = @"AfterYear";
NSString* const kAfterYearMonthPaymentKey = @"AfterYearMonthPayment";
NSString* const kAfterMonthDecreaseKey = @"AfterMonthDecrease";

@implementation ZYMortagageCalculateUtil
+ (instancetype)shareInstance{
    static ZYMortagageCalculateUtil *shareInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        shareInstance = [[self alloc] init];
    });
    
    return shareInstance;
}
/**
 计算等额本息贷款
 
 @param totalAmount 贷款总额
 @param loanAmount 商业贷金额
 @param loanRate 商业贷利率
 @param loanYear 商业贷年限
 @param fundAmount 公积金贷款金额
 @param fundRate 公积金贷款利率
 @param fundYear 公积金贷款年限
 @return 结果
 */
-(NSDictionary*)calculateAverageCapitPlusInterestWithTotalLoanAmount:(NSInteger)totalAmount loanAmount:(NSInteger)loanAmount loanRate:(CGFloat)loanRate loanYear:(NSInteger)loanYear fundAmount:(NSInteger)fundAmount fundRate:(CGFloat)fundRate fundYear:(NSInteger)fundYear{
    NSMutableDictionary* dictFinalResult;//最终计算结果
    NSMutableDictionary* dictLoanResult;//商业贷款计算结果
    NSMutableDictionary* dictFundResult;//公积金贷款给计算结果
    if (loanAmount>0) {
        dictLoanResult = [NSMutableDictionary dictionaryWithDictionary:[self calculateAverageCapitPlusInterestWithAmount:loanAmount rate:loanRate year:loanYear]];
    }
    if (fundAmount>0) {
        dictFundResult = [NSMutableDictionary dictionaryWithDictionary:[self calculateAverageCapitPlusInterestWithAmount:fundAmount rate:fundRate year:fundYear]];
    }
    if(!dictLoanResult&&!dictFundResult){
        return nil;
    }else{
        if(dictLoanResult&&!dictFundResult){
            //只有商业贷款
            dictFinalResult = [NSMutableDictionary dictionaryWithDictionary:dictLoanResult];
        }else if(!dictLoanResult&&dictFundResult){
            //只有公积金贷款
            dictFinalResult = [NSMutableDictionary dictionaryWithDictionary:dictFundResult];
        }else if(dictLoanResult&&dictFundResult){
            //组合贷
            dictFinalResult = [NSMutableDictionary dictionary];
            CGFloat totalInterest = [[dictLoanResult objectForKey:kTotalInterestKey] floatValue]+[[dictFundResult objectForKey:kTotalInterestKey] floatValue];//总利息
            [dictFinalResult setObject:@(totalInterest) forKey:kTotalInterestKey];
            
            NSArray* arrayLoan = [dictLoanResult objectForKey:kArrayMortgageKey];
            NSArray* arrayFund = [dictFundResult objectForKey:kArrayMortgageKey];
            
            NSInteger loanArrayCount = [arrayLoan count];
            NSInteger fundArrayCount = [arrayFund count];
            
            NSArray* finalArray = loanArrayCount>=fundArrayCount?arrayLoan:arrayFund;
            NSArray* waitArray = loanArrayCount>=fundArrayCount?arrayFund:arrayLoan;
            if(loanArrayCount!=fundArrayCount){
                //两边年限不对等,则需要分别记录不同年限前后的金额差别
                //记录后X年的月供
                ZYMortgageModel* finalModel = [finalArray lastObject];
                ZYMortgageModel* waitModel = [waitArray lastObject];
                [dictFinalResult setObject:@((finalModel.month - waitModel.month)/12) forKey:kAfterYearKey];
                [dictFinalResult setObject:@(finalModel.monthPayment) forKey:kAfterYearMonthPaymentKey];
                
            }
            
            for (NSInteger index = 0; index<[waitArray count]; index++) {
                ZYMortgageModel* waitModel = [waitArray objectAtIndex:index];
                ZYMortgageModel* finalModel = [finalArray objectAtIndex:index];
                finalModel.monthPaymentCapital += waitModel.monthPaymentCapital;
                finalModel.monthPaymentInterest += waitModel.monthPaymentInterest;
                finalModel.monthPayment += waitModel.monthPayment;
                finalModel.surplusCapital += waitModel.surplusCapital;
                if(loanArrayCount!=fundArrayCount){
                    //两边年限不对等,则需要分别记录不同年限前后的金额差别
                    //记录前X年的月供
                    if (index == ([waitArray count]-1)) {
                        [dictFinalResult setObject:@(finalModel.month/12) forKey:kBeforeYearKey];
                        [dictFinalResult setObject:@(finalModel.monthPayment) forKey:kBeforeYearMonthPaymentKey];
                    }
                }else{
                    if (index==0) {
                        [dictFinalResult setObject:@(finalModel.monthPayment) forKey:kMonthPayment];
                    }
                }
            }
            [dictFinalResult setObject:finalArray forKey:kArrayMortgageKey];
        }
        [dictFinalResult setObject:@(totalAmount) forKey:kTotalAmountKey];
        return dictFinalResult;
    }
}
/**
 计算等额本金贷款
 
 @param totalAmount 贷款总额
 @param loanAmount 商业贷金额
 @param loanRate 商业贷利率
 @param loanYear 商业贷年限
 @param fundAmount 公积金贷款金额
 @param fundRate 公积金贷款利率
 @param fundYear 公积金贷款年限
 @return 结果
 */
-(NSDictionary*)calculateAverageCapitalWithTotalLoanAmount:(NSInteger)totalAmount loanAmount:(NSInteger)loanAmount loanRate:(CGFloat)loanRate loanYear:(NSInteger)loanYear fundAmount:(NSInteger)fundAmount fundRate:(CGFloat)fundRate fundYear:(NSInteger)fundYear{
    NSMutableDictionary* dictFinalResult;//最终计算结果
    NSMutableDictionary* dictLoanResult;//商业贷款计算结果
    NSMutableDictionary* dictFundResult;//公积金贷款给计算结果
    if (loanAmount>0) {
        dictLoanResult = [NSMutableDictionary dictionaryWithDictionary:[self calculateAverageCapitalWithAmount:loanAmount rate:loanRate year:loanYear]];
    }
    if (fundAmount>0) {
        dictFundResult = [NSMutableDictionary dictionaryWithDictionary:[self calculateAverageCapitalWithAmount:fundAmount rate:fundRate year:fundYear]];
    }
    if(!dictLoanResult&&!dictFundResult){
        return nil;
    }else{
        if(dictLoanResult&&!dictFundResult){
            //只有商业贷款
            dictFinalResult = [NSMutableDictionary dictionaryWithDictionary:dictLoanResult];
        }else if(!dictLoanResult&&dictFundResult){
            //只有公积金贷款
            dictFinalResult = [NSMutableDictionary dictionaryWithDictionary:dictFundResult];
        }else if(dictLoanResult&&dictFundResult){
            //组合贷
            dictFinalResult = [NSMutableDictionary dictionary];
            CGFloat totalInterest = [[dictLoanResult objectForKey:kTotalInterestKey] floatValue]+[[dictFundResult objectForKey:kTotalInterestKey] floatValue];//总利息
            [dictFinalResult setObject:@(totalInterest) forKey:kTotalInterestKey];
            
            NSArray* arrayLoan = [dictLoanResult objectForKey:kArrayMortgageKey];
            NSArray* arrayFund = [dictFundResult objectForKey:kArrayMortgageKey];
            
            CGFloat loanDecrease =[[dictLoanResult objectForKey:kMonthDecreaseKey] floatValue];
            CGFloat fundDecrease =[[dictFundResult objectForKey:kMonthDecreaseKey] floatValue];
            
            NSInteger loanArrayCount = [arrayLoan count];
            NSInteger fundArrayCount = [arrayFund count];
            NSArray* finalArray = loanArrayCount>=fundArrayCount?arrayLoan:arrayFund;
            NSArray* waitArray = loanArrayCount>=fundArrayCount?arrayFund:arrayLoan;
            if(loanArrayCount!=fundArrayCount){
                //两边年限不对等,则需要分别记录不同年限前后的金额差别
                //记录后X年的月供
                ZYMortgageModel* finalModel = [finalArray lastObject];
                ZYMortgageModel* waitModel = [waitArray lastObject];

                [dictFinalResult setObject:@((finalModel.month-waitModel.month)/12) forKey:kAfterYearKey];
                [dictFinalResult setObject:@(finalModel.monthPayment) forKey:kAfterYearMonthPaymentKey];
                
                if (loanArrayCount>fundArrayCount) {
                    [dictFinalResult setObject:@(loanDecrease) forKey:kAfterMonthDecreaseKey];
                }else{
                    [dictFinalResult setObject:@(fundDecrease) forKey:kAfterMonthDecreaseKey];
                }
            }else{
                [dictFinalResult setObject:@(loanDecrease+fundDecrease) forKey:kMonthDecreaseKey];
            }
            
            for (NSInteger index = 0; index<[waitArray count]; index++) {
                ZYMortgageModel* waitModel = [waitArray objectAtIndex:index];
                ZYMortgageModel* finalModel = [finalArray objectAtIndex:index];
                finalModel.monthPaymentCapital += waitModel.monthPaymentCapital;
                finalModel.monthPaymentInterest += waitModel.monthPaymentInterest;
                finalModel.monthPayment += waitModel.monthPayment;
                finalModel.surplusCapital += waitModel.surplusCapital;
                if (index == 0){
                    if(loanArrayCount!=fundArrayCount){
                        //两边年限不对等,则需要分别记录不同年限前后的金额差别
                        //首月月供
                        [dictFinalResult setObject:@(finalModel.monthPayment) forKey:kBeforeYearMonthPaymentKey];
                    }else{
                        [dictFinalResult setObject:@(finalModel.monthPayment) forKey:kMonthPayment];
                    }
                }
                if(loanArrayCount!=fundArrayCount){
                    //两边年限不对等,则需要分别记录不同年限前后的金额差别
                    //记录前X年的月供
                    
                    if (index == ([waitArray count]-1)) {
                        [dictFinalResult setObject:@(finalModel.month/12) forKey:kBeforeYearKey];
                        [dictFinalResult setObject:@(fundDecrease+loanDecrease) forKey:kBeforeMonthDecreaseKey];
                    }
                }
            }
            [dictFinalResult setObject:finalArray forKey:kArrayMortgageKey];
        }
        [dictFinalResult setObject:@(totalAmount) forKey:kTotalAmountKey];
        return dictFinalResult;
    }
}
/**
 计算等额本息贷款
 
 @param amount 金额
 @param rate 利率
 @param year 年限
 @return 计算结果
 */
-(NSDictionary*)calculateAverageCapitPlusInterestWithAmount:(NSInteger)amount rate:(CGFloat)rate year:(NSInteger)year{
    //每月月供额=〔贷款本金×月利率×(1＋月利率)＾还款月数〕÷〔(1＋月利率)＾还款月数-1〕
    //每月应还利息=贷款本金×月利率×〔(1+月利率)^还款月数-(1+月利率)^(还款月序号-1)〕÷〔(1+月利率)^还款月数-1〕
    //每月应还本金=贷款本金×月利率×(1+月利率)^(还款月序号-1)÷〔(1+月利率)^还款月数-1〕
    //总利息=还款月数×每月月供额-贷款本金
    
    CGFloat monthRate = rate/12;//月利率
    CGFloat monthNum = year*12;//还款月数
    CGFloat monthPayment = (amount*monthRate*pow((1+monthRate),monthNum))/(pow((1+monthRate),monthNum)-1) ;//月供
    CGFloat totalInterest = monthNum*monthPayment-amount;/**< 总利息 */
    CGFloat surplusCapital = amount;//剩余本金
    NSMutableArray* arrayMortgage = [NSMutableArray arrayWithCapacity:monthNum];
    for(NSInteger month = 1;month<=monthNum;month++){
        ZYMortgageModel* model = [[ZYMortgageModel alloc] init];
        model.month = month;
        model.monthPaymentCapital = amount*monthRate*pow((1+monthRate),month-1)/(pow((1+monthRate),monthNum)-1);//月供本金
        model.monthPaymentInterest = amount*monthRate*(pow((1+monthRate),monthNum)-pow((1+monthRate),month-1))/(pow((1+monthRate),monthNum)-1);//月供利息;
        model.monthPayment = monthPayment;
        surplusCapital = surplusCapital - model.monthPaymentCapital;
        if (surplusCapital<0) {
            surplusCapital = 0;
        }
        model.surplusCapital = surplusCapital;
        [arrayMortgage addObject:model];
    }
    NSMutableDictionary* dictResult = [NSMutableDictionary dictionary];
    [dictResult setObject:@(totalInterest) forKey:kTotalInterestKey];
    [dictResult setObject:arrayMortgage forKey:kArrayMortgageKey];
    [dictResult setObject:@(monthPayment) forKey:kMonthPayment];
    
    return dictResult;
}
/**
 计算等额本金贷款

 @param amount 金额
 @param rate 利率
 @param year 年限
 @return 计算结果
 */
-(NSDictionary*)calculateAverageCapitalWithAmount:(NSInteger)amount rate:(CGFloat)rate year:(NSInteger)year{
    //每月月供额=(贷款本金÷还款月数)+(贷款本金-已归还本金累计额)×月利率
    //每月应还本金=贷款本金÷还款月数
    //每月应还利息=剩余本金×月利率=(贷款本金-已归还本金累计额)×月利率
    //每月月供递减额=每月应还本金×月利率=贷款本金÷还款月数×月利率
    //总利息=（还款月数+1）*贷款额*月利率/2
    
    CGFloat monthRate = rate/12;//月利率
    CGFloat monthNum = year*12;//还款月数
    CGFloat monthCapital = amount*1.0f/monthNum;//月供本金
    CGFloat monthDecrease = monthCapital*monthRate;//每月月供递减
    CGFloat totalInterest = (monthNum+1)*amount*monthRate/2;/**< 总利息 */
    CGFloat surplusCapital = amount;//剩余本金
    NSMutableDictionary* dictResult = [NSMutableDictionary dictionary];

    NSMutableArray* arrayMortgage = [NSMutableArray arrayWithCapacity:monthNum];
    for(NSInteger month = 1;month<=monthNum;month++){
        ZYMortgageModel* model = [[ZYMortgageModel alloc] init];
        model.month = month;
        model.monthPaymentCapital = monthCapital;//月供本金
        surplusCapital = surplusCapital - model.monthPaymentCapital;
        if (surplusCapital<0) {
            surplusCapital = 0;
        }
        model.surplusCapital = surplusCapital;
        model.monthPaymentInterest = surplusCapital*monthRate;//月供利息;
        model.monthPayment = model.monthPaymentCapital + model.monthPaymentInterest;
        [arrayMortgage addObject:model];
        if (month==1) {
            [dictResult setObject:@(model.monthPayment) forKey:kMonthPayment];
        }
    }
    [dictResult setObject:@(totalInterest) forKey:kTotalInterestKey];
    [dictResult setObject:@(monthDecrease) forKey:kMonthDecreaseKey];
    [dictResult setObject:arrayMortgage forKey:kArrayMortgageKey];
    return dictResult;
}

@end
