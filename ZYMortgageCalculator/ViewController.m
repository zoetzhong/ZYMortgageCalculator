//
//  ViewController.m
//  ZYMortgageCalculator
//
//  Created by 钟煜 on 2019/12/20.
//  Copyright © 2019 zoet. All rights reserved.
//

#import "ViewController.h"
#import "ZYMortgageCalculatorViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(IBAction)onButtonClicked:(id)sender{
    ZYMortgageCalculatorViewController* vc = [[ZYMortgageCalculatorViewController alloc] initWithNibName:@"ZYMortgageCalculatorViewController" bundle:nil];
    [vc setTotalPrice:200 loanRate:0.049f fundRate:0.0325f];//初始值：200万，商业贷款利率：4.9%，公积金贷款利率：3.25%
    [self.navigationController pushViewController:vc animated:YES];
}

@end
