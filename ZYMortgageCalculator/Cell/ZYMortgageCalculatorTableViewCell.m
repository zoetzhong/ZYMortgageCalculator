//
//  ZYMortgageCalculatorTableViewCell.m
//  ZYMortgageCalculator
//
//  Created by Zoet on 2017/3/22.
//  Copyright © 2017年 wzwl. All rights reserved.
//

#import "ZYMortgageCalculatorTableViewCell.h"

@implementation ZYMortgageCalculatorTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    //单位图标
//    UIColor* darkGreen = [UIColor colorWithRed:3.0f/255.0f green:177.0f/255.0f blue:96.0f/255.0f alpha:1.0f];
    UILabel* labPrice = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 22,18)];
    labPrice.text = @" 万";
    labPrice.font = [UIFont systemFontOfSize:15.0f];

//    labPrice.textColor = darkGreen;
//    labPrice.textAlignment = NSTextAlignmentCenter;
//    labPrice.layer.cornerRadius = 3.0f;
//    labPrice.layer.borderColor = darkGreen.CGColor;
//    labPrice.layer.borderWidth = 1.0f;
    
    _fieldPrice.rightView = labPrice;
    _fieldPrice.rightViewMode = UITextFieldViewModeAlways;
    
    if(_isInputFloat){
        _fieldPrice.keyboardType = UIKeyboardTypeDecimalPad;
    }
    
}
- (void)closeKeyboard{
    [_fieldPrice resignFirstResponder];
}

/**
 修改输入框单位为%
 */
-(void)setRateUnitView{
    UILabel* labRate = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 22,18)];
    labRate.text = @" %";
    labRate.font = [UIFont systemFontOfSize:15.0f];
    _fieldPrice.rightView = labRate;
    _fieldPrice.rightViewMode = UITextFieldViewModeAlways;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)onDoneButtonClicked:(id)sender{
    [_fieldPrice resignFirstResponder];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    UIToolbar *bar = [[UIToolbar alloc] initWithFrame:CGRectMake(0,0, self.frame.size.width,44)];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - 60, 7,50, 30)];
    [button setTitle:@"完成"forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithRed:3.0f/255.0f green:177.0f/255.0f blue:96.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(onDoneButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bar addSubview:button];
    textField.inputAccessoryView = bar;
    if ([textField.text isEqualToString:@"0"]) {
        textField.text = @"";
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [_fieldPrice resignFirstResponder];
    
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if(_isInputFloat){
        if(self.delegate&&[self.delegate respondsToSelector:@selector(didUpdateRateValue:)]){
            [self.delegate didUpdateRateValue:[textField.text floatValue]];
        }
    }else{
        if(self.delegate&&[self.delegate respondsToSelector:@selector(didUpdateValue:)]){
            [self.delegate didUpdateValue:[textField.text integerValue]];
        }
    }
    
}
@end
