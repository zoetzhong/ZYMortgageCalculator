//
//  ZYCalculateResultTableViewCell.m
//  ZYMortgageCalculator
//
//  Created by Zoet on 2017/3/23.
//  Copyright © 2017年 wzwl. All rights reserved.
//

#import "ZYCalculateResultTableViewCell.h"

@implementation ZYCalculateResultTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    
}

/**
 等额本息，无时间差

 @param downPaymen 首付
 @param totalLoanAmount 贷款总额
 @param totalInterest 利息总额
 @param monthPayment 月供
 */
-(void)setDownPaymen:(NSInteger)downPaymen totalLoanAmount:(NSInteger)totalLoanAmount totalInterest:(NSInteger)totalInterest monthPayment:(CGFloat)monthPayment{
    _labMoreLineTitle1.text = @"月供";
    [_labMoreLineTitle2 setHidden:YES];
    [_labMoreLineTitle3 setHidden:YES];
    [_labMoreLineTitle4 setHidden:YES];
    
    _labDownPaymen.text = [NSString stringWithFormat:@"%ld万元",downPaymen];
    _labTotalLoanAmount.text = [NSString stringWithFormat:@"%ld万元",totalLoanAmount];
    _labTotalInterest.text = [NSString stringWithFormat:@"%ld万元",totalInterest];
    _labMoreLineValue1.text = [NSString stringWithFormat:@"%.2f元/月",monthPayment];
    [_labMoreLineValue2 setHidden:YES];
    [_labMoreLineValue3 setHidden:YES];
    [_labMoreLineValue4 setHidden:YES];
}

/**
等额本息，有时间差

 @param downPaymen 首付
 @param totalLoanAmount 贷款总额
 @param totalInterest 利息总额
 @param beforeYear 前几年
 @param beforeMonthPayment 前几年月供
 @param afterYear 后几年
 @param afterMonthPayment 后几年月供
 */
-(void)setDownPaymen:(NSInteger)downPaymen totalLoanAmount:(NSInteger)totalLoanAmount totalInterest:(NSInteger)totalInterest beforeYear:(NSInteger)beforeYear beforeMonthPayment:(CGFloat)beforeMonthPayment afterYear:(NSInteger)afterYear afterMonthPayment:(CGFloat)afterMonthPayment{
    _labMoreLineTitle1.text = [NSString stringWithFormat:@"前%ld年月供",beforeYear];
    [_labMoreLineTitle2 setHidden:NO];
    _labMoreLineTitle2.text = [NSString stringWithFormat:@"后%ld年月供",afterYear];
    [_labMoreLineTitle3 setHidden:YES];
    [_labMoreLineTitle4 setHidden:YES];
    
    _labDownPaymen.text = [NSString stringWithFormat:@"%ld万元",downPaymen];
    _labTotalLoanAmount.text = [NSString stringWithFormat:@"%ld万元",totalLoanAmount];
    _labTotalInterest.text = [NSString stringWithFormat:@"%ld万元",totalInterest];
    _labMoreLineValue1.text = [NSString stringWithFormat:@"%.2f元/月",beforeMonthPayment];
    [_labMoreLineValue2 setHidden:NO];
    _labMoreLineValue2.text = [NSString stringWithFormat:@"%.2f元/月",afterMonthPayment];
    [_labMoreLineValue3 setHidden:YES];
    [_labMoreLineValue4 setHidden:YES];
}
/**
 等额本金，无时间差
 
 @param downPaymen 首付
 @param totalLoanAmount 贷款总额
 @param totalInterest 利息总额
 @param monthPayment 月供
 @param monthDecrease 月递减
 */
-(void)setDownPaymen:(NSInteger)downPaymen totalLoanAmount:(NSInteger)totalLoanAmount totalInterest:(NSInteger)totalInterest monthPayment:(CGFloat)monthPayment monthDecrease:(CGFloat)monthDecrease{
    _labMoreLineTitle1.text = @"首月月供";
    [_labMoreLineTitle2 setHidden:NO];
    _labMoreLineTitle2.text = @"每月递减";
    [_labMoreLineTitle3 setHidden:YES];
    [_labMoreLineTitle4 setHidden:YES];
    
    _labDownPaymen.text = [NSString stringWithFormat:@"%ld万元",downPaymen];
    _labTotalLoanAmount.text = [NSString stringWithFormat:@"%ld万元",totalLoanAmount];
    _labTotalInterest.text = [NSString stringWithFormat:@"%ld万元",totalInterest];
    _labMoreLineValue1.text = [NSString stringWithFormat:@"%.2f元/月",monthPayment];
    [_labMoreLineValue2 setHidden:NO];
    _labMoreLineValue2.text = [NSString stringWithFormat:@"%.2f元",monthDecrease];
    [_labMoreLineValue3 setHidden:YES];
    [_labMoreLineValue4 setHidden:YES];

}
/**
 等额本金，有时间差
 
 @param downPaymen 首付
 @param totalLoanAmount 贷款总额
 @param totalInterest 利息总额
 @param beforeMonthPayment 首月月供
 @param beforeDecrease 首月递减
 @param afterMonth 后期第几个月
 @param afterMonthPayment 后几年月供
 @param afterDecrease 后几年递减
*/
-(void)setDownPaymen:(NSInteger)downPaymen totalLoanAmount:(NSInteger)totalLoanAmount totalInterest:(NSInteger)totalInterest beforeMonthPayment:(CGFloat)beforeMonthPayment beforeDecrease:(CGFloat)beforeDecrease afterMonth:(NSInteger)afterMonth afterMonthPayment:(CGFloat)afterMonthPayment afterDecrease:(CGFloat)afterDecrease{
    _labMoreLineTitle1.text = @"首月月供";
    [_labMoreLineTitle2 setHidden:NO];
    _labMoreLineTitle2.text = @"每月递减";
    [_labMoreLineTitle3 setHidden:NO];
    _labMoreLineTitle3.text = [NSString stringWithFormat:@"第%ld月月供",afterMonth];
    [_labMoreLineTitle4 setHidden:NO];
    _labMoreLineTitle4.text = @"每月递减";
    
    _labDownPaymen.text = [NSString stringWithFormat:@"%ld万元",downPaymen];
    _labTotalLoanAmount.text = [NSString stringWithFormat:@"%ld万元",totalLoanAmount];
    _labTotalInterest.text = [NSString stringWithFormat:@"%ld万元",totalInterest];
    _labMoreLineValue1.text = [NSString stringWithFormat:@"%.2f元/月",beforeMonthPayment];
    [_labMoreLineValue2 setHidden:NO];
    _labMoreLineValue2.text = [NSString stringWithFormat:@"%.2f元",beforeDecrease];
    [_labMoreLineValue3 setHidden:NO];
    _labMoreLineValue3.text = [NSString stringWithFormat:@"%.2f元/月",afterMonthPayment];
    [_labMoreLineValue4 setHidden:NO];
    _labMoreLineValue4.text = [NSString stringWithFormat:@"%.2f元",afterDecrease];
}
-(IBAction)onSegmentValueChange:(id)sender{
    UISegmentedControl* segment = sender;
    
    if(self.delegate&&[self.delegate respondsToSelector:@selector(didChangeSegmentToIndex:)]){
        [self.delegate didChangeSegmentToIndex:segment.selectedSegmentIndex];
    }
}
-(IBAction)onDetailButtonClicked:(id)sender{
    if(self.delegate&&[self.delegate respondsToSelector:@selector(didSelectedCheckDetail)]){
        [self.delegate didSelectedCheckDetail];
    }
}
@end
