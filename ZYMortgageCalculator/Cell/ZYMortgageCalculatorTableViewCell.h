//
//  ZYMortgageCalculatorTableViewCell.h
//  ZYMortgageCalculator
//
//  Created by Zoet on 2017/3/22.
//  Copyright © 2017年 wzwl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZYMortgageCalculatorTableViewCellDelegate <NSObject>
@optional
-(void)didUpdateValue:(NSInteger)value;
-(void)didUpdateRateValue:(CGFloat)rateValue;
@end

@interface ZYMortgageCalculatorTableViewCell : UITableViewCell<UITextFieldDelegate>
@property (weak, nonatomic) id<ZYMortgageCalculatorTableViewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel* labTitle;/**< 标题 */
@property (weak, nonatomic) IBOutlet UILabel* labContent;/**< 内容 */
@property (weak, nonatomic) IBOutlet UITextField* fieldPrice;/**< 价格 */

@property (assign, nonatomic) BOOL isInputFloat;/**< 是否是输入小数点 */
- (void)closeKeyboard;
/**
 修改输入框单位为%
 */
-(void)setRateUnitView;
@end
