//
//  ZYMortgageDetailTableViewCell.h
//  ZYMortgageCalculator
//
//  Created by Zoet on 2017/3/27.
//  Copyright © 2017年 wzwl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZYMortgageDetailTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel* labMonth;/**< 期数 */
@property (weak, nonatomic) IBOutlet UILabel* labPaymentCapital;/**< 月供本金 */
@property (weak, nonatomic) IBOutlet UILabel* labPaymentInterest;/**< 月供利息 */
@property (weak, nonatomic) IBOutlet UILabel* labPayment;/**< 月供 */
@property (weak, nonatomic) IBOutlet UILabel* labSurplusCapital;/**< 剩余本金 */
@end
