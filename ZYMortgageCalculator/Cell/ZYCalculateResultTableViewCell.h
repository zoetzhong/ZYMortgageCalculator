//
//  ZYCalculateResultTableViewCell.h
//  ZYMortgageCalculator
//
//  Created by Zoet on 2017/3/23.
//  Copyright © 2017年 wzwl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZYCalculateResultTableViewCellDelegate <NSObject>

-(void)didChangeSegmentToIndex:(NSInteger)index;
-(void)didSelectedCheckDetail;

@end

@interface ZYCalculateResultTableViewCell : UITableViewCell
@property (strong, nonatomic) id<ZYCalculateResultTableViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UISegmentedControl* segment;/**< 贷款类型 */
@property (weak, nonatomic) IBOutlet UILabel* labDownPaymen;/**< 首付 */
@property (weak, nonatomic) IBOutlet UILabel* labTotalLoanAmount;/**< 贷款总额 */
@property (weak, nonatomic) IBOutlet UILabel* labTotalInterest;/**< 利息总额 */
@property (weak, nonatomic) IBOutlet UILabel* labMoreLineTitle1;/**< 额外标题1 */
@property (weak, nonatomic) IBOutlet UILabel* labMoreLineTitle2;/**< 额外标题2 */
@property (weak, nonatomic) IBOutlet UILabel* labMoreLineTitle3;/**< 额外标题3 */
@property (weak, nonatomic) IBOutlet UILabel* labMoreLineTitle4;/**< 额外标题4 */
@property (weak, nonatomic) IBOutlet UILabel* labMoreLineValue1;/**< 额外值1 */
@property (weak, nonatomic) IBOutlet UILabel* labMoreLineValue2;/**< 额外值2 */
@property (weak, nonatomic) IBOutlet UILabel* labMoreLineValue3;/**< 额外值3 */
@property (weak, nonatomic) IBOutlet UILabel* labMoreLineValue4;/**< 额外值4 */


/**
 等额本息，无时间差
 
 @param downPaymen 首付
 @param totalLoanAmount 贷款总额
 @param totalInterest 利息总额
 @param monthPayment 月供
 */
-(void)setDownPaymen:(NSInteger)downPaymen totalLoanAmount:(NSInteger)totalLoanAmount totalInterest:(NSInteger)totalInterest monthPayment:(CGFloat)monthPayment;

/**
 等额本息，有时间差
 
 @param downPaymen 首付
 @param totalLoanAmount 贷款总额
 @param totalInterest 利息总额
 @param beforeYear 前几年
 @param beforeMonthPayment 前几年月供
 @param afterYear 后几年
 @param afterMonthPayment 后几年月供
 */
-(void)setDownPaymen:(NSInteger)downPaymen totalLoanAmount:(NSInteger)totalLoanAmount totalInterest:(NSInteger)totalInterest beforeYear:(NSInteger)beforeYear beforeMonthPayment:(CGFloat)beforeMonthPayment afterYear:(NSInteger)afterYear afterMonthPayment:(CGFloat)afterMonthPayment;
/**
 等额本金，无时间差
 
 @param downPaymen 首付
 @param totalLoanAmount 贷款总额
 @param totalInterest 利息总额
 @param monthPayment 月供
 @param monthDecrease 月递减
 */
-(void)setDownPaymen:(NSInteger)downPaymen totalLoanAmount:(NSInteger)totalLoanAmount totalInterest:(NSInteger)totalInterest monthPayment:(CGFloat)monthPayment monthDecrease:(CGFloat)monthDecrease;
/**
 等额本金，有时间差
 
 @param downPaymen 首付
 @param totalLoanAmount 贷款总额
 @param totalInterest 利息总额
 @param beforeMonthPayment 首月月供
 @param beforeDecrease 首月递减
 @param afterMonth 后期第几个月
 @param afterMonthPayment 后几年月供
 @param afterDecrease 后几年递减
 */
-(void)setDownPaymen:(NSInteger)downPaymen totalLoanAmount:(NSInteger)totalLoanAmount totalInterest:(NSInteger)totalInterest beforeMonthPayment:(CGFloat)beforeMonthPayment beforeDecrease:(CGFloat)beforeDecrease afterMonth:(NSInteger)afterMonth afterMonthPayment:(CGFloat)afterMonthPayment afterDecrease:(CGFloat)afterDecrease;

@end
