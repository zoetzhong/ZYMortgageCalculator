# 房贷计算器

#### 介绍
该房贷计算器是两年前一个项目的其中一个模块，当时为了实现这一功能，在网上没发现有合适的开源项目，于是只能自己造轮子。时隔两年，偶然想起这个计算器，决定将其开源。

计算器功能简单，可自定义参数进行计算，并列出明细。

#### 使用说明

1.  将项目中的Cell、Model、ViewController三个文件夹中的文件加入到自己的项目中
2.  在需要跳转计算器的VC中以Nib的方式初始化计算器VC
3.  传入初始参数

（2、3步参考项目中的ViewController.m文件的内容）

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
